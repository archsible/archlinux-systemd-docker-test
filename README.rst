==========================================
Archlinux Docker image with systemd and ssh for tests
==========================================

.. image:: https://img.shields.io/gitlab/pipeline/archsible/archlinux-systemd-docker-test/master
        :target: https://gitlab.com/archsible/archlinux-systemd-docker-test/pipelines


*archlinux* docker image with systemd and ssh enabled for CI tests

* Free software: MIT license


Features
--------

* docker image with systemd and ssh for tests


Usage
--------------

.. code-block:: console

    $ docker pull image registry.gitlab.com/archsible/archlinux-systemd-docker-test:latest


from a `Dockerfile` :

first generate a ssh key-pair,

.. code-block:: console

    $ ssh-keygen -t rsa -f id_rsa


Then use the following Dockerfile as an example,

.. code-block:: console

    FROM registry.gitlab.com/archsible/archlinux-systemd-docker-test:latest
    COPY id_rsa.pub /root/.ssh/authorized_keys


Credits
-------

* TODO

