FROM registry.gitlab.com/archsible/archlinux-systemd-docker-base:latest
RUN pacman -Sy && pacman -Su --noconfirm base-devel ansible && pacman -Scc --noconfirm

RUN echo "%wheel ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
RUN groupadd dockergroup
RUN useradd -G wheel -g dockergroup -m -d /home/dockeruser -s /bin/bash dockeruser

# adds default key for tests
COPY id_rsa.pub /home/dockeruser/.ssh/authorized_keys
WORKDIR /home/dockeruser
USER dockeruser
